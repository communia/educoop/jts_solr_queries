Aquest mdoul depen de upstream a que responguin a https://www.drupal.org/project/search_api_location/issues/3113266 per tal de possibiltar
la indexació de poligons.
* Amb aquest mòdul fem un override del plugin de rpt que limita la indexació de solr a lat,lon enlloc de poder guardar poligons. Ens subscribim
a un event de search_api datatypes i sobreescribim la classe del datatype rpt per fer servir la nostra propia classe que no filtra per parell
lat,lon i permet més shapes.

* A part a modul veiem com modificar una cerca en un índex perpenjar-nos al hook  jts_solr_queries_search_api_solr_converted_query_alter Que modifica el select de les cerques, 
de moment només la devents per veure si un punt està dins d'un polígon

* A part afegeix a la configuració schema.xml que genera el search_api_solr la
  configuració per consultes espacials:
  ```
    <fieldType name="location_rpt" class="solr.SpatialRecursivePrefixTreeFieldType"
            spatialContextFactory="org.locationtech.spatial4j.context.jts.JtsSpatialContextFactory"
        autoIndex="true"
        validationRule="repairBuffer0"
        distErrPct="0.025" maxDistErr="0.001" distanceUnits="kilometers" />
```
enlloc de:
```
<fieldType name="location_rpt" class="solr.SpatialRecursivePrefixTreeFieldType" geo="true" distErrPct="0.025" maxDistErr="0.001" distanceUnits="kilometers"/>
```
